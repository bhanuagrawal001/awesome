package com.dabblepoint.awesome.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.dabblepoint.awesome.dto.AppsInfo;
import com.dabblepoint.awesome.utils.AppsRepository;
import com.dabblepoint.awesome.utils.MyApp;

import java.util.ArrayList;

import javax.inject.Inject;

public class AppsViewModel extends AndroidViewModel {

    private MutableLiveData<AppsInfo> mCurrentApps;
    @Inject AppsRepository appsRepository;

    public AppsViewModel(@NonNull Application application) {
        super(application);
        ((MyApp)application).getLocalDataComponent().inject(this);
    }

    public MutableLiveData<AppsInfo> getAppsInfo() {

        if (mCurrentApps == null) {
            mCurrentApps = new MutableLiveData<AppsInfo>();
            appsRepository.fetchApps(mCurrentApps);
        }
        return mCurrentApps;
    }

}
