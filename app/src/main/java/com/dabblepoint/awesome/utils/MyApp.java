package com.dabblepoint.awesome.utils;

import android.app.Application;

public class MyApp extends Application {

    private LocalDataComponent localDataComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        localDataComponent = DaggerLocalDataComponent.builder()
                .appModule(new AppModule(this))
                .localDataModule(new LocalDataModule())
                .build();
    }

    public LocalDataComponent getLocalDataComponent() {
        return localDataComponent;
    }
}
