package com.dabblepoint.awesome.utils;

public class Constants {
    public static final int ALL_APPS_COLUMN_COUNT = 4;
    public static final int APPS_PER_PAGE = 12;
}
