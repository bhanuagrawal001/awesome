package com.dabblepoint.awesome.utils;
import com.dabblepoint.awesome.dto.AppDTO;
import com.dabblepoint.awesome.dto.AppsInfo;

import java.util.ArrayList;

public class AppUtils {

    public static ArrayList<AppDTO> getApps(AppsInfo appsInfo, int position) {
        return new ArrayList<AppDTO>(appsInfo.getApps().subList(position*appsInfo.getApps_per_page(), appsInfo.getApps().size()>position*appsInfo.getApps_per_page() + appsInfo.getApps_per_page()?  position*appsInfo.getApps_per_page() + appsInfo.getApps_per_page(): appsInfo.getApps().size()));
    }

    public static int getNoOfPages(AppsInfo appsInfo) {
        return (int) Math.ceil((float)appsInfo.getApps().size()/appsInfo.getApps_per_page());
    }
}
