package com.dabblepoint.awesome.utils;

import com.dabblepoint.awesome.viewmodels.AppsViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, LocalDataModule.class})
public interface LocalDataComponent {
    void inject(AppsViewModel appsViewModel);
}
