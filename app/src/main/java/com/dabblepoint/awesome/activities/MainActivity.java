package com.dabblepoint.awesome.activities;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dabblepoint.awesome.R;
import com.dabblepoint.awesome.fragments.AppList;
import com.dabblepoint.awesome.fragments.Apps;

public class MainActivity extends AppCompatActivity implements AppList.OnFragmentInteractionListener,
        Apps.OnFragmentInteractionListener{

    private static final String APP_LIST_FRAGMENT = "APPS_FRAGMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.fragment_container, AppList.newInstance("", ""), MainActivity.APP_LIST_FRAGMENT).
                commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
